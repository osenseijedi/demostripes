<%@ include file="/WEB-INF/stripes/layout/taglibs.jsp" %>

<s:layout-render name="/WEB-INF/stripes/layout/layout.jsp">
	<s:layout-component name="contents">
		
		<h1>Editing</h1>
		<s:form beanclass="${actionBean.className }">
			<s:hidden name="id" />
			
			<p>
				Property1 must never be equal to 'Bobby'.
			</p>
			
			<sdyn:text name="property1" placeholder="Enter value for property1"/><br/>
			<sdyn:text name="property2" placeholder="Enter value for property2"/>
			
			<s:submit name="save">Save</s:submit>
		</s:form>
		
		<h1>List</h1>
		
		<c:if test="${fn:length(actionBean.models) le 0 }">
			No model saved yet...
		</c:if>
		<c:if test="${fn:length(actionBean.models) gt 0 }">
			<table>
				<tr>
					<th>id</th>
					<th>Property 1</th>
					<th>Property 2</th>
				</tr>
				<c:forEach items="${actionBean.models }" var="model">
					<tr>
						<td>
							<s:link beanclass="${actionBean.className }">
								${model.id }
								<s:param name="id" value="${model.id }" />
							</s:link>
						</td>
						<td>${model.property1 }</td>
						<td>${model.property2 }</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>

		
	</s:layout-component>
</s:layout-render>
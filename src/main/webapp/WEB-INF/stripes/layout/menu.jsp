<%@ include file="/WEB-INF/stripes/layout/taglibs.jsp" %>

<nav class="w3-topnav">
	<c:set var="currentUrl" value="${actionBean.urlBinding }"/>

	<s:link beanclass="stripesdemo.action.HomeActionBean"   class="${ currentUrl eq '/home.ext'  ? 'w3-theme-action w3-card-2' : ''}">Home</s:link>
	<s:link beanclass="stripesdemo.action.Page1ActionBean"  class="${ currentUrl eq '/page1.ext' ? 'w3-theme-action w3-card-2' : ''}">Page 1</s:link>
	<s:link beanclass="stripesdemo.action.Page2ActionBean"  class="${ currentUrl eq '/page2.ext' ? 'w3-theme-action w3-card-2' : ''}">Page 2</s:link>
	<s:link beanclass="stripesdemo.action.ModelActionBean"  class="${ currentUrl eq '/model.ext'  ? 'w3-theme-action w3-card-2' : ''}">Model Form</s:link>
</nav>
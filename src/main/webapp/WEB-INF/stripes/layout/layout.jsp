<%@ include file="/WEB-INF/stripes/layout/taglibs.jsp" %>
<%@ page trimDirectiveWhitespaces="true" %>
<s:layout-definition>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
<!-- 	<meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<link rel="icon" href="img/favicon.ico">
	
	<link rel="stylesheet" href="lib/w3css/w3.css" type="text/css" />
	<link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css"/>
	
	<link rel="stylesheet" href="css/w3-custom.css" type="text/css" />
	<link rel="stylesheet" href="css/w3-theme-darcula.css" type="text/css" />
	
	<s:layout-component name="htmlHead"/>
</head>
<body class="w3-theme-d4">

	<div id="page">
		<header >
			<div  class="w3-container">
				<div class="w3-row">
					<div class="w3-col s12 m3">
						<h2>
							A Site Title
						</h2>
					</div>
					<div class="w3-col s12 m8"> 
						<jsp:include page="menu.jsp"/>
					</div>
				</div>
			</div>
		</header>
		
		<section id="pageSection" class="w3-theme-d3 w3-card-4" >
			<div class="w3-container ">
				<div class="w3-row">
					<div class="w3-col m1 l2">
						&nbsp;
					</div>
					<div class="w3-col m10 l8">
						<s:messages/>
						<s:errors/>
					</div>
				</div>
			</div>
			<div  class="w3-container ">
				<div class="w3-row">
					<div class="w3-col m1 l2">
						&nbsp;
					</div>
					<div class="w3-col m10 l8">
						<article class="w3-container">
							<s:layout-component name="contents"/>
						</article>
					</div>
				</div>
			</div>
		</section>
	
	</div>  <!-- end of div page -->
	
	<div id="pageFooter" class="w3-container">
		
		<div  class="w3-container">
			<div class="w3-row">
				<div class="w3-col m1 l2">
					&nbsp;
				</div>
				<div class="w3-col m10 l8">
					<jsp:include page="footer.jsp"/>
				</div>
			</div>
		</div>
		
	</div>
</body>
</html>
</s:layout-definition>
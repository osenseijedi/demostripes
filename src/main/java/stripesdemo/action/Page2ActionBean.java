package stripesdemo.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

@UrlBinding("/page2.ext")
public class Page2ActionBean extends BaseActionBean{
	private static String JSP = "/WEB-INF/stripes/page2.jsp";
	
	@DefaultHandler
	public Resolution defaultAction() {
		return new ForwardResolution(JSP);
	}
}

package stripesdemo.action;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.SimpleMessage;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.SimpleError;

public abstract class BaseActionBean implements ActionBean {
	protected Logger log = Logger.getLogger(BaseActionBean.class);

	private ActionBeanContext context;

	public ActionBeanContext getContext() {return context;}
	public void setContext(ActionBeanContext context) {this.context = context;}

	/**
	 * Just a simple shortcut to the current session. (only here to make
	 * expressions shorter). - Doesn't Make any test if there is a context OR a
	 * request -
	 */
	protected HttpSession getSession() {
		return getContext().getRequest().getSession();
	}
	
	public String getTitle() {
		return "Stripes Demo";
	}
	
	public final String getUrlBinding() {
		if (getClass() != null && getClass().getAnnotation(UrlBinding.class) != null) {
			return getClass().getAnnotation(UrlBinding.class).value();
		}
		return null;
	}

	
	public void addError(String error){
		getContext().getValidationErrors().addGlobalError(new SimpleError(error));
	}
	
	public void addMessage(String message){
		getContext().getMessages().add(new SimpleMessage(message));
	}
	
	public final String getClassName() {
		return this.getClass().getName();
	}
	
	@DefaultHandler
	public abstract Resolution defaultAction();
	
	
}
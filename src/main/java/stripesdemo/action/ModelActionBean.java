package stripesdemo.action;

import java.util.List;

import net.sourceforge.stripes.action.Before;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.RedirectResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;
import net.sourceforge.stripes.validation.Validate;
import net.sourceforge.stripes.validation.ValidationErrorHandler;
import net.sourceforge.stripes.validation.ValidationErrors;
import net.sourceforge.stripes.validation.ValidationMethod;
import stripesdemo.model.Model;
import stripesdemo.model.SomeRepo;

@UrlBinding("/model.ext")
public class ModelActionBean extends BaseActionBean implements ValidationErrorHandler{
	private static String JSP = "/WEB-INF/stripes/model.jsp";
	
	@Before(on="defaultAction")
	public void load(){
		if(getId() != null){
			Model model = SomeRepo.getInstance().get(getId());
			setProperty1(model.getProperty1());
			setProperty2(model.getProperty2());
		}
	}
	
	@DefaultHandler
	public Resolution defaultAction() {
		return new ForwardResolution(JSP);
	}
	
	//Custom validation -- happens AFTER automatic validation
	@ValidationMethod(on="save")
	public void validateSave(){
		if("Bobby".equals(getProperty1())){
			addError("Nein! Nein! This is an error!!!");
		}
	}
	
	public Resolution save(){
		Model model = new Model();
		
		if(getId()!= null)
			model = SomeRepo.getInstance().get(getId());
		
		model.setProperty1(getProperty1());
		model.setProperty2(getProperty2());
		
		SomeRepo.getInstance().saveOrUpdate(model);
		
		addMessage("Success!!");
		
		return new RedirectResolution(this.getUrlBinding()).addParameter("id", getId());
	}
	
	public Resolution handleValidationErrors(ValidationErrors errors) throws Exception {
		//in case of error, nothing special, we go back to the default action
		return defaultAction();
	}
	
	private Long id;
	private String property1;
	private String property2;
	
	public Long getId() { return id; }
	public void setId(Long id) { this.id = id; }
	
	//Automatic validation when saving.
	@Validate(on="save", required=true)
	public String getProperty1() {return property1;}
	public void setProperty1(String property1) {this.property1 = property1;}

	@Validate(on="save", required=true)
	public String getProperty2() {return property2;}
	public void setProperty2(String property2) {this.property2 = property2;}

	public List<Model> getModels(){
		return SomeRepo.getInstance().getAllModels();
	}
}

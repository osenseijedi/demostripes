package stripesdemo.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

@UrlBinding("/home.ext")
public class HomeActionBean extends BaseActionBean {
	private static String JSP = "/WEB-INF/stripes/home.jsp";
	
	@DefaultHandler
	public Resolution defaultAction() {
		return new ForwardResolution(JSP);
	}
}

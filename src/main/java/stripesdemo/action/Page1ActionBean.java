package stripesdemo.action;

import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;
import net.sourceforge.stripes.action.UrlBinding;

@UrlBinding("/page1.ext")
public class Page1ActionBean extends BaseActionBean{
	private static String JSP = "/WEB-INF/stripes/page1.jsp";
	
	@DefaultHandler
	public Resolution defaultAction() {
		return new ForwardResolution(JSP);
	}
}

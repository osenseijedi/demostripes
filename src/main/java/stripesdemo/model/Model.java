package stripesdemo.model;

public class Model {
	
	private Long id;
	private String property1;
	private String property2;
	
	public Model(){};
	
	public Long getId() { return id; }
	/*package*/ void setId(Long id) { this.id = id; }
	
	public String getProperty1() { return property1; }
	public void setProperty1(String property1) { this.property1 = property1; }
	
	public String getProperty2() { return property2; }
	public void setProperty2(String property2) { this.property2 = property2; }

}


package stripesdemo.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SomeRepo {
	
	private static SomeRepo instance;
	private SomeRepo(){};
	
	public static SomeRepo getInstance(){
		if(instance == null){
			instance = new SomeRepo();
		}
		return instance;
	}
	
	private Map<Long, Model> dbTable = new HashMap<Long, Model>();
	
	public Model get(Long id){
		if(id == null)
			return null;
		
		return dbTable.get(id);
	}
	
	public void saveOrUpdate(Model model){
		if(model == null)
			return;
		if(model.getId() == null){
			model.setId(new Long(dbTable.size()));
		}
		dbTable.put(model.getId(), model);
	}
	
	public List<Model> getAllModels(){
		return new ArrayList<Model>(dbTable.values());
	}
	
}
